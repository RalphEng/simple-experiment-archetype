@echo ******************
@echo prepare execute archetype
@echo ******************

pushd c:\temp

@echo ******************
@echo create clean execute directory
@echo ******************

rd /S /Q newProject
md newProject
cd newProject

@echo ******************
@echo execute archetype
@echo ******************

@echo ******************
@echo hint: a typical experiment project GAV is
@echo groupId:             de.humanfork.experiment.xyz
@echo artifactId:          experiment-xyz
@echo gitlab-project-name: experiment-xyz
@echo ******************


call mvn org.apache.maven.plugins:maven-archetype-plugin:3.2.1:generate -DarchetypeCatalog=local -DarchetypeGroupId=de.humanfork.mvnarchetype.simpleexperiment -DarchetypeArtifactId=simple-experiment-archetype

@echo ******************
@echo show result
@echo ******************

start explorer c:\temp\newProject
popd
