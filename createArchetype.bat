@echo ******************
@echo create archetype
@echo ******************

call mvn clean 
call mvn org.apache.maven.plugins:maven-archetype-plugin:3.1.2:create-from-project -Darchetype.properties=archetype.properties -DaddDefaultExcludes=false
call mvn org.apache.maven.plugins:maven-antrun-plugin:3.0.0:run@archtype-pom-post-process -PpostByAnt

@echo ******************
@echo install archetype
@echo ******************

pushd target\generated-sources\archetype
call mvn install
popd
 