Maven Archetype for an simple experiment project
================================================

This is a project to generate a Maven Archetype for simple experiments (none multimodule).
The archetype is generated from this blueprint project via `mvn archetype:create-from-project` and some postprocessing.

Notes about Maven Archetypes can be found in [project Wiki:Maven-Archetype](https://gitlab.com/RalphEng/simple-experiment-archetype/wikis/Maven-Archetype)

Usage
-----

The archetype is currently not available in Maven Central.
But this blueprint contains a script to generate the archetype and install it in the local Maven repository:

    createArchetype.bat
    
Afterwards one can use

    mvn archetype:generate -DarchetypeCatalog=local
  
  or

    executeArchetype.bat
    
to create a new project from this archetype.
The archetype group and artifact id are:
- groupId: `de.humanfork.mvnarchetype.simpleexperiment`
- artifactId: `simple-experiment-archetype`

When creating a new project, maven will ask for: `gitlab-project-name`, this is the Name of the project in your Git Lab Account.


> **NOTE**
> Projects created from this archetype will have an unintended Windows Ling Ending (CRLF).
> But the git attributes are configured to fix this problem while committing the files.
> So it is strongly recommend to start working with correct files (LF)
> Either by push the created project to git, then delete it locally, and clone the project afterwards or by fixing the local workspace.
> The init-git-repo-script follows the 2. approach.

The created project contains a batch file to setup a git repository

    initGitRepo.bat
  
This batch script will ask some questions how to initialize.


Naming
------

- Blueprint:
  This is the Blueprint (Project), it is a runnable Maven project.
  It looks like the project that is created at the end.
- Archetype:
  An archetype is the Maven Project that could be used to create new projects with `mvn archetype:generate`.
  An archetype consists of 3 major elements:
    - Its pom, the archetype-pom, that mostly contains the Maven GAV of the archetype.
      Attention: this is **not** the pom that is used as prototype for the created project  
    - An folder `src\main\resources\archtype-resources` that contains the archetype prototype
    - An file `src\main\resources\META-INF-\maven\archetype-metadata.xml` that contains the archetype descriptor
- Prototype:
  The prototype is some kind of template, that could contain some kind of placeholders that get filled when the real project become created. 
- generated Project:
  The project created form the archetype
  
```
                                        +------------------+   generate              +-----------+
                                        | Archetype        |------------------------>| generated |
                                        |                  | mvn archetype:generate  | project   |
+-----------+        base for           |    +-----------+ |                         +-----------+
| Blueprint | --------------------------+--> | Prototype | |
+-----------+  mvn archetype:generate   |    +-----------+ |
               and some postprocessing  |                  |
                                        +------------------+
```

### Maven GAV:
Artifact Id: `de.humanfork.mvnarchetype.simpleexperiment`
Group Ids:
- Blueprint: `simple-experiment-blueprint` _The blueprint is normally not going to been deployed to a global Maven Repository_
- Archetype: `simple-experiment-archetype`


This Blueprint
--------------

This Blueprint project contains the Blueprint and some code for post-processing to create the Prototype in a fully automated way.

This contains 3 batch files:
- `createArchetype.bat` create the archetype (`de.humanfork.mvnarchetype.simpleexperiment:simple-experiment-archetype`) from this blueprint and install and it locally
- `executeArchetype.bat` can be used the execute the generated the project from the archetype 
- `createAndExecuteArchetype.bat` run both batch files


generated project
-----------------

The generated project will have a GitLab CI file, that run `mvn test`, based on https://gitlab.com/RalphEng/simple-experiment-pipeline.
