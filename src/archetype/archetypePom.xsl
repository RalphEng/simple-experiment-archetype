<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:mvn="http://maven.apache.org/POM/4.0.0"
	exclude-result-prefixes="mvn">
	
	<!--
		add maven-resources-plugin with addDefaultExcludes=false to pluginManagement of archtetype pom.xml
	-->
	
	<!-- Identity transform -->
	<xsl:template match="@* | node()">	
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- add the maven-resources-plugin after the last plugin in the plugin-management-section  -->
	<xsl:template match="/mvn:project/mvn:build/mvn:pluginManagement/mvn:plugins/mvn:plugin[last()]">
		<xsl:copy-of select="."/>
		<plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-resources-plugin</artifactId>
			<version>3.1.0</version>
			<configuration>
				<!-- copy .gitignore and .gitattributes too -->
				<addDefaultExcludes>false</addDefaultExcludes>
			</configuration>
		</plugin>
  	</xsl:template>

</xsl:transform>