<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="https://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.1.0"
	xmlns:atd="https://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.1.0"
	exclude-result-prefixes="atd">

	<!-- Identity transform -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<!-- add required property: version with default value 0.0.1-SNAPSHOT -->
	<xsl:template
		match="/atd:archetype-descriptor/atd:requiredProperties/atd:requiredProperty[last()]">
		<xsl:copy-of select="." />
		<requiredProperty key="version">
			<defaultValue>0.0.1-SNAPSHOT</defaultValue>
		</requiredProperty>
	</xsl:template>
	<xsl:template
		match="/atd:archetype-descriptor[not (atd:requiredProperties)]">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
			<requiredProperties>
				<requiredProperty key="version">
					<defaultValue>0.0.1-SNAPSHOT</defaultValue>
				</requiredProperty>
			</requiredProperties>
		</xsl:copy>
	</xsl:template>

</xsl:transform>