<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:mvn="http://maven.apache.org/POM/4.0.0"
	exclude-result-prefixes="mvn">
	
	<!--
		add maven-resources-plugin with addDefaultExcludes=false to pluginManagement of archtetype pom.xml
	-->
	
	<!-- Identity transform -->
	<xsl:template match="@* | node()">	
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- remove 'postByAnt' profile, and if it is the only profile, then remove the complete profiles block --> 
	<xsl:template match="/mvn:project/mvn:profiles[count(mvn:profile)=1 and mvn:profile/mvn:id[contains(.,'postByAnt')]]"/>
	<xsl:template match="/mvn:project/mvn:profiles[count(mvn:profile)>1]/mvn:profile[mvn:id[contains(.,'postByAnt')]]"/>

</xsl:transform>