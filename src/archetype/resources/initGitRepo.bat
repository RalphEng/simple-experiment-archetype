@echo off

@echo on
git init
@echo off

:loopUserNameEmail
choice /C YNA /m "config user.name and user.email? (Yes/No/Abort)"
if errorlevel 255 exit
if errorlevel 4 goto :loopUserNameEmail
if errorlevel 3 exit
if errorlevel 2 goto :skipUserNameEmail
if errorlevel 1 goto :configUserNameEmail

:configUserNameEmail
@echo on
git config user.name "Ralph Engelmann"
git config user.email "ralph@rshc.de"
@echo off

:skipUserNameEmail

:loopHttpsSsh
choice /C SHA /m "use SSH or HTTPS? (Ssl/Https/Abort)"
if errorlevel 255 exit
if errorlevel 4 goto :loopUserNameEmail
if errorlevel 3 exit
if errorlevel 2 goto :https
if errorlevel 1 goto :ssl


:https
@echo on
git remote add origin  https://gitlab.com/RalphEng/${gitlab-project-name}.git
@echo off
goto :commit

:ssl
@echo on
git remote add origin git@gitlab.com:RalphEng/${gitlab-project-name}.git
@echo off
goto :commit


:commit
@echo on
git add .
git reset -- initGitRepo.bat
git commit -m "Initial commit"
rem apply normalized eol
git rm --cached -r . 
git reset --hard
git push -u origin master

del initGitRepo.bat
@echo off